/* eslint-disable jsx-a11y/alt-text */

import React, {Fragment} from "react";
import Header from "./components/Header";
import imagesecurity from './styles/images/imagesecurity.jpeg';

function App() {
  return (
    <Fragment>
        <Header />
        <main>
            <span className={"intro"}>
                {/* Thanks for Watching <br />
                <span className={"intro__focus"}>Like</span> &
                <span className={"intro__focus"}> Subscribe</span>
                <br />
                for more! */}
                <img src={imagesecurity}/>
            </span>
        </main>
    </Fragment>
  );
}

export default App;
