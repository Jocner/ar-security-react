/* eslint-disable jsx-a11y/alt-text */
import React, { useState } from 'react'
import styles from './Header.module.scss';
import LOGO_ALTA_WHITE from '../styles/images/LOGO_ALTA_WHITE.jpg';

import { BiMenuAltRight } from 'react-icons/bi';
import { AiOutlineCloseSquare } from 'react-icons/ai';

const Header = () => {
    const [menuOpen, setMenuOpen] = useState(false);
    const menuToggler = () => setMenuOpen((p) => !p);
  return (
    <div className={styles.header}>
            <div className={styles.header__content}>
                <div>
                    <span className={styles.logo}><img src={LOGO_ALTA_WHITE}/></span>
                </div>
                <div>
                    <nav className={`${styles.nav} ${menuOpen ? styles[`nav--open`] : {}}`}>
                        <a className={styles.nav__item} href={"/"}>
                            Page One
                        </a>
                        <a className={styles.nav__item} href={"/"}>
                            Page Two
                        </a>
                        <a className={styles.nav__item} href={"/"}>
                            Page Three
                        </a>
                        <div className={styles.nav__button__container}>
                            <Button />
                        </div>
                    </nav>
                </div>
                <div>
                    <div className={styles.header__button__container}>
                        <Button />
                    </div>
                    <button className={styles.header__toggler} onClick={menuToggler}>
                        {!menuOpen ? <BiMenuAltRight /> : <AiOutlineCloseSquare />}
                    </button>
                </div>
            </div>
        </div>
  );
};

const Button = () => {
    return <button className={styles.button}>Click me</button>;
};

export default Header